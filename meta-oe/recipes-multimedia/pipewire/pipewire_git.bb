SUMMARY = "Multimedia processing server for Linux"
AUTHOR = "Wim Taymans <wtaymans@redhat.com>"
HOMEPAGE = "https://pipewire.org"
SECTION = "multimedia"
LICENSE = "MIT"
LIC_FILES_CHKSUM = " \
    file://LICENSE;md5=e2c0b7d86d04e716a3c4c9ab34260e69 \
    file://COPYING;md5=97be96ca4fab23e9657ffa590b931c1a \
"
DEPENDS = "alsa-lib dbus udev"
SRCREV = "aee694fb8260914b6dd6b12cb95e78dece204535"
PV = "0.3.20"

SRC_URI = "git://github.com/PipeWire/pipewire"

S = "${WORKDIR}/git"

inherit meson pkgconfig systemd manpages

PACKAGECONFIG ??= "\
    ${@bb.utils.contains('DISTRO_FEATURES', 'bluetooth', 'bluez', '', d)} \
    ${@bb.utils.filter('DISTRO_FEATURES', 'systemd vulkan', d)} \
    pipewire-alsa alsa jack gstreamer \
"

PACKAGECONFIG[bluez] = "-Dbluez5=true,-Dbluez5=false,bluez5 sbc"
PACKAGECONFIG[jack] = "-Djack=true,-Djack=false,jack"
PACKAGECONFIG[gstreamer] = "-Dgstreamer=true,-Dgstreamer=false,glib-2.0 gstreamer1.0 gstreamer1.0-plugins-base"
PACKAGECONFIG[manpages] = "-Dman=true,-Dman=false,libxml-parser-perl-native"
PACKAGECONFIG[systemd] = "-Dsystemd=true,-Dsystemd=false,systemd"
PACKAGECONFIG[vulkan] = "-Dvulkan=true,-Dvulkan=false,vulkan-loader"
PACKAGECONFIG[alsa] = "-Dalsa=true,-Dalsa=false"
PACKAGECONFIG[pipewire-alsa] = "-Dpipewire-alsa=true,-Dpipewire-alsa=false"

# Disable the `pipewire-jack` feature unconditionally as it causes the error
# 'Multiple shlib providers for libjack.so.0'.
EXTRA_OEMESON += "-Dpipewire-jack=false"

LDFLAGS_append_mipsarch = " -latomic"
LDFLAGS_append_x86 = " -latomic"
LDFLAGS_append_riscv32 = " -latomic"

do_install_append() {
    if ! echo ${PACKAGECONFIG} | grep -qw "jack"; then
        rm "${D}${sysconfdir}/pipewire/media-session.d/with-jack"
    fi
}

PACKAGES =+ "\
    ${PN}-spa-plugins \
    ${PN}-alsa \
    gstreamer1.0-${PN} \
    lib${PN} \
    lib${PN}-modules \
"

RDEPENDS_lib${PN} += "lib${PN}-modules ${PN}-spa-plugins"

FILES_${PN} = "\
    ${sysconfdir}/pipewire \
    ${bindir}/pw-* \
    ${bindir}/pipewire* \
    ${systemd_user_unitdir} \
"
FILES_lib${PN} = "\
    ${libdir}/libpipewire-*.so.* \
    ${libdir}/libpulse-*.so.* \
"
FILES_lib${PN}-modules = "\
    ${libdir}/pipewire-* \
"
FILES_${PN}-spa-plugins = "\
    ${bindir}/spa-* \
    ${libdir}/spa-* \
"
FILES_${PN}-alsa = "\
    ${libdir}/alsa-lib \
    ${datadir}/alsa \
    ${datadir}/alsa-card-profile \
    ${base_libdir}/udev/rules.d/90-pipewire-alsa.rules \
"
FILES_gstreamer1.0-${PN} = "\
    ${libdir}/gstreamer-1.0 \
"

CONFFILES_${PN} = "\
    ${sysconfdir}/pipewire/pipewire.conf \
    ${sysconfdir}/pipewire/media-session.d/alsa-monitor.conf \
    ${sysconfdir}/pipewire/media-session.d/bluez-monitor.conf \
    ${sysconfdir}/pipewire/media-session.d/v4l2-monitor.conf \
    ${sysconfdir}/pipewire/media-session.d/media-session.conf \
    ${@bb.utils.contains('PACKAGECONFIG','jack','${sysconfdir}/pipewire/media-session.d/with-jack','',d)} \
    ${sysconfdir}/pipewire/media-session.d/with-pulseaudio \
"
